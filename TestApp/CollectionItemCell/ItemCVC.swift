//
//  ItemCVC.swift
//  TestApp
//
//  Created by rakhmatillo on 11/03/24.
//

import UIKit

protocol CollectionViewItemDelegate{
    func didItemTapped(item: Int)
    func didItemReleased(item: Int)
}

class ItemCVC: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    static var ID : String {
        "ItemCVC"
    }
    static func Nib() -> UINib{
        return UINib(nibName: "ItemCVC", bundle: nil)
    }

    @IBOutlet weak var numberLabel: UILabel!
    var itemIndex : Int = 0
    var delegate : CollectionViewItemDelegate!
    var longGesture : UILongPressGestureRecognizer = UILongPressGestureRecognizer()
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = UIColor.green.cgColor
        contentView.layer.cornerRadius = 10
        
    }
    
    @objc func longtapped(_ gesture: UILongPressGestureRecognizer){
        if gesture.state == .began {
            UIView.animate(withDuration: 0.5) {
                self.contentView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            }
        }
        if gesture.state == .ended{
            UIView.animate(withDuration: 0.5) {
                self.contentView.transform = .identity
            }
        }
    }
    
    func updateCell(n : Int){
        contentView.addGestureRecognizer(longGesture)
        longGesture.delegate  = self
        longGesture.minimumPressDuration = 0.1
        longGesture.delaysTouchesBegan = true
        longGesture.numberOfTouchesRequired = 1
        longGesture.addTarget(self, action: #selector(longtapped(_:)))
        itemIndex = n
        numberLabel.text = "\(n)"
    }
}
