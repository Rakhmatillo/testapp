//
//  ViewController.swift
//  TestApp
//
//  Created by rakhmatillo on 11/03/24.
//

import UIKit

class ViewController: UIViewController {
    
    var tableView : UITableView! {
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.register(TableItemTVC.Nib(), forCellReuseIdentifier: TableItemTVC.ID)
        }
    }
  
    var data : [[Int]] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        loadData()
        setTableView()
        
    }
    
    private func setTableView(){
        tableView = UITableView(frame: self.view.frame)
        self.view.addSubview(tableView)
    }
    
    
    private func loadData(){
        for _ in (0...120){
            var row = [Int]()
            for _ in (0...(10...14).randomElement()!){
                row.append((1...10).randomElement()!)
            }
            data.append(row)
        }
    }
}
extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableItemTVC.ID, for: indexPath) as! TableItemTVC
        cell.updateCell(data: data[indexPath.row], row: indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        120
    }
    
}

