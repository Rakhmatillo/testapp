//
//  TableItemTVC.swift
//  TestApp
//
//  Created by rakhmatillo on 11/03/24.
//

import UIKit

class TableItemTVC: UITableViewCell {
    static var ID : String{
        "TableItemTVC"
    }
    static func Nib() -> UINib{
        return UINib(nibName: "TableItemTVC", bundle: nil)
    }
    
    var horizontalCollectionView : UICollectionView! {
        didSet{
            horizontalCollectionView.delegate = self
            horizontalCollectionView.dataSource = self
            horizontalCollectionView.register(ItemCVC.Nib(), forCellWithReuseIdentifier: ItemCVC.ID)
        }
    }
    var data : [Int] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    
    }
    
    private func setCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = .init(top: 0, left: 10, bottom: 0, right: 10)
        
        horizontalCollectionView = UICollectionView(frame: self.contentView.frame, collectionViewLayout: layout)
        self.contentView.addSubview(horizontalCollectionView)
        horizontalCollectionView.collectionViewLayout = layout
        horizontalCollectionView.showsHorizontalScrollIndicator = false
        
    }
    
    func updateCell(data : [Int], row : Int){
        setCollectionView()
        self.data = data
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { t in
            let cells = self.horizontalCollectionView.visibleCells as! [ItemCVC]
            if !cells.isEmpty{
                let cell = cells.randomElement()!
                cell.numberLabel.text = "\((22...55).randomElement()!)"
            }
        }.fire()
    }
    
    
}

extension TableItemTVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCVC.ID, for: indexPath) as? ItemCVC else{ return UICollectionViewCell()}
        cell.updateCell(n: data[indexPath.item])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
}
